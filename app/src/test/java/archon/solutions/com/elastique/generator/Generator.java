package archon.solutions.com.elastique.generator;

import java.util.ArrayList;
import java.util.List;

public abstract class Generator<T> {
    protected int counter = 1;

    public abstract T next();

    public List<T> getList(int numberOfElements) {
        List<T> result = new ArrayList<>(numberOfElements);

        for (int i = 0; i < numberOfElements; i++) {
            result.add(next());
        }

        return result;
    }

    public void resetCounter() {
        counter = 1;
    }
}
