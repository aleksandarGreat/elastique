package archon.solutions.com.elastique.repository.remote.converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Publisher;
import archon.solutions.com.elastique.generator.repository.PublisherDtoGenerator;
import archon.solutions.com.elastique.repository.remote.dto.PublisherDto;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PublisherDtoToPublisherConverterTest {

    private PublisherDtoGenerator publisherDtoGenerator = new PublisherDtoGenerator();

    @Before
    public void setup() {
        publisherDtoGenerator.resetCounter();
    }

    @Test
    public void testPublisherDtoToPublisherConverter() {
        // Arrange
        PublisherDto publisherDto = publisherDtoGenerator.next();

        // Act
        Publisher publisher = PublisherDtoToPublisherConverter.publisherDtoToPublisher(publisherDto);

        // Assert
        assertThat(publisher.getId(), equalTo(publisherDto.getId()));
        assertThat(publisher.getId(), equalTo(publisherDto.getId()));
        assertThat(publisher.getId(), equalTo(publisherDto.getId()));
    }

    @Test
    public void testPublisherDtosToPublishersConverter() {
        // Arrange
        List<PublisherDto> publisherDtos = publisherDtoGenerator.getList(3);

        // Act
        List<Publisher> publishers = PublisherDtoToPublisherConverter.publisherDtosToPublishers(publisherDtos);

        // Assert
        assertThat(publishers.size(), equalTo(publisherDtos.size()));
        assertThat(publishers.get(0).getId(), equalTo(publisherDtos.get(0).getId()));
        assertThat(publishers.get(1).getId(), equalTo(publisherDtos.get(1).getId()));
        assertThat(publishers.get(2).getId(), equalTo(publisherDtos.get(2).getId()));
    }
}
