package archon.solutions.com.elastique.repository.remote.converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.generator.repository.BookDtoGenerator;
import archon.solutions.com.elastique.generator.repository.BookDtoListGenerator;
import archon.solutions.com.elastique.repository.remote.dto.BookDto;
import archon.solutions.com.elastique.repository.remote.dto.BookDtoList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BookDtoToBookConverterTest {

    private final BookDtoGenerator bookDtoGenerator = new BookDtoGenerator();

    private final BookDtoListGenerator bookDtoListGenerator = new BookDtoListGenerator(3);

    @Before
    public void setup() {
        bookDtoGenerator.resetCounter();
    }

    @Test
    public void testPublisherDtoToPublisherConverter() {
        // Arrange
        BookDto bookDto = bookDtoGenerator.next();

        // Act
        Book book = BookDtoToBookConverter.convertBookDtoToBook(bookDto);

        // Assert
        assertThat(book.getId(), equalTo(bookDto.getId()));
        assertThat(book.getId(), equalTo(bookDto.getId()));
        assertThat(book.getId(), equalTo(bookDto.getId()));
    }

    @Test
    public void testBookDtosToBooksConverter() {
        // Arrange
        List<BookDto> bookDtos = bookDtoGenerator.getList(3);

        // Act
        List<Book> books = BookDtoToBookConverter.convertBookDtosToBooks(bookDtos);

        // Assert
        assertThat(books.size(), equalTo(bookDtos.size()));
        assertThat(books.get(0).getId(), equalTo(bookDtos.get(0).getId()));
        assertThat(books.get(1).getId(), equalTo(bookDtos.get(1).getId()));
        assertThat(books.get(2).getId(), equalTo(bookDtos.get(2).getId()));
    }

    @Test
    public void testPublisherDtosToPublishersConverter() {
        // Arrange
        List<BookDto> bookDtos = bookDtoGenerator.getList(3);

        // Act
        List<Book> books = BookDtoToBookConverter.convertBookDtosToBooks(bookDtos);

        // Assert
        assertThat(books.size(), equalTo(bookDtos.size()));
        assertThat(books.get(0).getId(), equalTo(bookDtos.get(0).getId()));
        assertThat(books.get(1).getId(), equalTo(bookDtos.get(1).getId()));
        assertThat(books.get(2).getId(), equalTo(bookDtos.get(2).getId()));
    }

    @Test
    public void testBookDtoListToBooksConverter() {
        // Arrange
        BookDtoList bookDtoList = bookDtoListGenerator.next();

        // Act
        List<Book> books = BookDtoToBookConverter.convertBooksListDtoToBooks(bookDtoList);

        // Assert
        assertThat(books.size(), equalTo(bookDtoList.getBooks().size()));
        assertThat(books.get(0).getId(), equalTo(bookDtoList.getBooks().get(0).getId()));
        assertThat(books.get(1).getId(), equalTo(bookDtoList.getBooks().get(1).getId()));
        assertThat(books.get(2).getId(), equalTo(bookDtoList.getBooks().get(2).getId()));
    }
}
