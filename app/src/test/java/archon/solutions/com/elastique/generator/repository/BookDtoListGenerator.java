package archon.solutions.com.elastique.generator.repository;

import archon.solutions.com.elastique.generator.Generator;
import archon.solutions.com.elastique.repository.remote.dto.BookDtoList;

public class BookDtoListGenerator extends Generator<BookDtoList> {

    private final BookDtoGenerator bookDtoGenerator = new BookDtoGenerator();

    private int numberOfBookDtos;

    public BookDtoListGenerator(int numberOfBookDtos) {
        this.numberOfBookDtos = numberOfBookDtos;
    }

    @Override
    public BookDtoList next() {
        return new BookDtoList(bookDtoGenerator.getList(numberOfBookDtos));
    }
}
