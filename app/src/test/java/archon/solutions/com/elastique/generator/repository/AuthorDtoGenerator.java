package archon.solutions.com.elastique.generator.repository;

import archon.solutions.com.elastique.generator.Generator;
import archon.solutions.com.elastique.repository.remote.dto.AuthorDto;

public class AuthorDtoGenerator extends Generator<AuthorDto> {

    @Override
    public AuthorDto next() {
        final int id = counter++;
        return new AuthorDto(id, "firstName" + id, "lastName" + id);
    }
}
