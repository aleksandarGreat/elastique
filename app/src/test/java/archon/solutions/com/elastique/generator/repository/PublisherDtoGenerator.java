package archon.solutions.com.elastique.generator.repository;

import archon.solutions.com.elastique.generator.Generator;
import archon.solutions.com.elastique.repository.remote.dto.PublisherDto;

public class PublisherDtoGenerator extends Generator<PublisherDto> {

    @Override
    public PublisherDto next() {
        final int id = counter++;
        return new PublisherDto(id, "name" + id);
    }
}
