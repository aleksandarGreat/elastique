package archon.solutions.com.elastique.generator.domain;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.generator.Generator;

public class BookGenerator extends Generator<Book> {

    @Override
    public Book next() {
        final int id = counter++;
        return new Book(id, "Title" + counter, "Description" + counter, "CoverUrl" + counter,
                "isbn" + counter);
    }
}
