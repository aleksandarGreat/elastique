package archon.solutions.com.elastique.usecase.repository.remote;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.generator.domain.BookGenerator;
import archon.solutions.com.elastique.usacase.repository.BookUseCase;
import archon.solutions.com.elastique.usacase.repository.dependency.remote.BookRemoteDao;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookUseCaseTest {

    @Mock
    private BookRemoteDao bookRemoteDao;

    private BookUseCase bookUseCase;

    private BookGenerator bookGenerator = new BookGenerator();

    @Before
    public void setup() {
        bookUseCase = new BookUseCase(bookRemoteDao);
        bookGenerator.resetCounter();
    }

    @Test
    public void testReturnHighlighted() {
        // Arrange
        final int numberOfBooks = 3;
        final List<Book> books = bookGenerator.getList(numberOfBooks);
        when(bookRemoteDao.readHighlightedBooks()).thenReturn(Flowable.just(books));

        // Act
        final TestSubscriber<List<Book>> testObserver = bookUseCase.readHighlightedBooks().test();
        testObserver.awaitTerminalEvent();
        final List<Book> testBooks = testObserver.values().get(0);

        // Assert
        testObserver.assertNoErrors().assertValue(testObserverBooks -> testObserverBooks.size() == books.size());
        assertThat(testBooks.get(0), equalTo(books.get(0)));
        assertThat(testBooks.get(1), equalTo(books.get(1)));
        assertThat(testBooks.get(2), equalTo(books.get(2)));
        verify(bookRemoteDao, times(1)).readHighlightedBooks();
    }

    @Test
    public void testReadBooksByQuery() {
        // Arrange
        final String query = "query";
        final int numberOfBooks = 3;
        final List<Book> books = bookGenerator.getList(numberOfBooks);
        when(bookRemoteDao.readBooksByQuery(query)).thenReturn(Flowable.just(books));

        // Act
        final TestSubscriber<List<Book>> testObserver = bookUseCase.readBooksByQuery(query).test();
        testObserver.awaitTerminalEvent();
        final List<Book> testBooks = testObserver.values().get(0);

        // Assert
        testObserver.assertNoErrors().assertValue(testObserverBooks -> testObserverBooks.size() == books.size());
        assertThat(testBooks.get(0), equalTo(books.get(0)));
        assertThat(testBooks.get(1), equalTo(books.get(1)));
        assertThat(testBooks.get(2), equalTo(books.get(2)));
        verify(bookRemoteDao, times(1)).readBooksByQuery(query);
    }
}
