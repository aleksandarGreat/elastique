package archon.solutions.com.elastique.repository.remote.converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Author;
import archon.solutions.com.elastique.generator.repository.AuthorDtoGenerator;
import archon.solutions.com.elastique.repository.remote.dto.AuthorDto;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AuthorDtoToAuthorConverterTest {

    private AuthorDtoGenerator authorDtoGenerator = new AuthorDtoGenerator();

    @Before
    public void setup() {
        authorDtoGenerator.resetCounter();
    }

    @Test
    public void testAuthorDtoToAuthorConverter() {
        // Arrange
        AuthorDto authorDto = authorDtoGenerator.next();

        // Act
        Author author = AuthorDtoToAuthorConverter.authorDtoToAuthorConverter(authorDto);

        // Assert
        assertThat(author.getId(), equalTo(authorDto.getId()));
        assertThat(author.getId(), equalTo(authorDto.getId()));
        assertThat(author.getId(), equalTo(authorDto.getId()));

    }

    @Test
    public void testAuthorDtosToAuthorsConverter() {
        // Arrange
        List<AuthorDto> authorDtos = authorDtoGenerator.getList(3);

        // Act
        List<Author> authors = AuthorDtoToAuthorConverter.authorDtosToAuthorsConverter(authorDtos);

        // Assert
        assertThat(authors.size(), equalTo(authorDtos.size()));
        assertThat(authors.get(0).getId(), equalTo(authorDtos.get(0).getId()));
        assertThat(authors.get(1).getId(), equalTo(authorDtos.get(1).getId()));
        assertThat(authors.get(2).getId(), equalTo(authorDtos.get(2).getId()));
    }
}