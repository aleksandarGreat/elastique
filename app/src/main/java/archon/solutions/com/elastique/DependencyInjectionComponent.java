package archon.solutions.com.elastique;

import javax.inject.Singleton;

import archon.solutions.com.elastique.repository.remote.RemoteRepositoryModule;
import archon.solutions.com.elastique.system.SystemModule;
import archon.solutions.com.elastique.ui.fragment.BooksFragment;
import archon.solutions.com.elastique.usacase.UseCaseModule;
import dagger.Component;

@Component(modules = {SystemModule.class, UseCaseModule.class, RemoteRepositoryModule.class})
@Singleton
public interface DependencyInjectionComponent {

    void inject(BooksFragment booksFragment);
}
