package archon.solutions.com.elastique.repository.remote.client;

public interface HttpClient {

    BackendApiService getBackendApiService();
}
