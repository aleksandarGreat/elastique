package archon.solutions.com.elastique.repository.remote.converter;

import java.util.ArrayList;
import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.repository.remote.dto.BookDto;
import archon.solutions.com.elastique.repository.remote.dto.BookDtoList;

public class BookDtoToBookConverter {

    public static List<Book> convertBooksListDtoToBooks(BookDtoList bookDtosList) {
        return convertBookDtosToBooks(bookDtosList.getBooks());
    }

    public static List<Book> convertBookDtosToBooks(List<BookDto> bookDtos) {
        List<Book> books = new ArrayList<>();

        for (BookDto bookDto : bookDtos) {
            books.add(convertBookDtoToBook(bookDto));
        }

        return books;
    }

    public static Book convertBookDtoToBook(BookDto bookDto) {
        return new Book(bookDto.getId(), bookDto.getTitle(), bookDto.getDescription(), bookDto.getCoverUrl(),
                bookDto.getIsbn(), AuthorDtoToAuthorConverter.authorDtoToAuthorConverter(bookDto.getAuthor()),
                PublisherDtoToPublisherConverter.publisherDtoToPublisher(bookDto.getPublisher()));
    }
}
