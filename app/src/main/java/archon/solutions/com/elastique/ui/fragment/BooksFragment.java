package archon.solutions.com.elastique.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.IgnoreWhen;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import archon.solutions.com.elastique.ElastiqueApplication;
import archon.solutions.com.elastique.R;
import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.ui.activity.BookActivity_;
import archon.solutions.com.elastique.ui.adapter.BooksAdapter;
import archon.solutions.com.elastique.ui.view.BookItemView;
import archon.solutions.com.elastique.usacase.repository.BookUseCase;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

@EFragment(R.layout.fragment_books)
public class BooksFragment extends BaseFragment implements Consumer<List<Book>>, BookItemView.BookActionListener {

    @App
    ElastiqueApplication application;

    @ViewById
    RecyclerView recyclerView;

    @Bean
    BooksAdapter booksAdapter;

    @Inject
    BookUseCase bookUseCase;

    private BooksFragmentListener booksFragmentListener;

    @AfterViews
    void initialize() {
        application.getDiComponent().inject(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(booksAdapter);

        booksAdapter.setBookActionListener(this);
        compositeDisposable.add(bookUseCase.readHighlightedBooks()
                .observeOn(AndroidSchedulers.mainThread()).subscribe(this,
                        this::onError,
                        this::onComplete));
    }

    @Override
    public void bookSelected(Book book) {
        BookActivity_.intent(this).book(book).start();
    }

    @IgnoreWhen(IgnoreWhen.State.VIEW_DESTROYED)
    @Override
    public void accept(List<Book> books) throws Exception {
        updateBooks(books);
    }

    public void readHighlightedBooks() {
        compositeDisposable.add(bookUseCase.readHighlightedBooks()
                .observeOn(AndroidSchedulers.mainThread()).subscribe(this,
                        this::onError,
                        this::onComplete));
    }

    public void readBooksByQuery(String query) {
        compositeDisposable.add(bookUseCase.readBooksByQuery(query)
                .observeOn(AndroidSchedulers.mainThread()).subscribe(this,
                        this::onError,
                        this::onComplete));
    }

    private void updateBooks(List<Book> books) {
        booksAdapter.setBooks(books);

        if (booksFragmentListener != null) {
            booksFragmentListener.booksUpdated(books);
        }
    }

    public void setBooksFragmentListener(BooksFragmentListener booksFragmentListener) {
        this.booksFragmentListener = booksFragmentListener;
    }

    /**
     * Used to handle book update.
     */
    public interface BooksFragmentListener {
        /**
         * Triggered once list of books is updated.
         *
         * @param books - Newly fetched list of books.
         */
        void booksUpdated(List<Book> books);
    }
}
