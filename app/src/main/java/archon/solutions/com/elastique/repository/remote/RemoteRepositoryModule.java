package archon.solutions.com.elastique.repository.remote;

import javax.inject.Singleton;

import archon.solutions.com.elastique.repository.remote.client.BackendApiService;
import archon.solutions.com.elastique.repository.remote.client.HttpClient;
import archon.solutions.com.elastique.repository.remote.client.RetrofitClient;
import archon.solutions.com.elastique.repository.remote.dao.BookRemoteDaoImpl;
import archon.solutions.com.elastique.repository.remote.dependency.NetworkManager;
import archon.solutions.com.elastique.usacase.repository.dependency.remote.BookRemoteDao;
import dagger.Module;
import dagger.Provides;

@Module
public class RemoteRepositoryModule {

    @Provides
    @Singleton
    HttpClient provideHttpClient(NetworkManager networkManager) {
        return new RetrofitClient(networkManager);
    }

    @Provides
    @Singleton
    BackendApiService provideBackendApiService(HttpClient httpClient) {
        return httpClient.getBackendApiService();
    }

    @Provides
    @Singleton
    BookRemoteDao providesBookRemoteDao(BackendApiService backendApiService) {
        return new BookRemoteDaoImpl(backendApiService);
    }
}
