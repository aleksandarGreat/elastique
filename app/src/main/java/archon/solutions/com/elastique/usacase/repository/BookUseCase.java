package archon.solutions.com.elastique.usacase.repository;

import java.util.List;

import javax.inject.Inject;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.usacase.repository.dependency.remote.BookRemoteDao;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class BookUseCase {

    private final BookRemoteDao bookRemoteDao;

    @Inject
    public BookUseCase(BookRemoteDao bookRemoteDao) {
        this.bookRemoteDao = bookRemoteDao;
    }

    public Flowable<List<Book>> readHighlightedBooks() {
        return bookRemoteDao.readHighlightedBooks().subscribeOn(Schedulers.io());
    }

    public Flowable<List<Book>> readBooksByQuery(String query) {
        return bookRemoteDao.readBooksByQuery(query).subscribeOn(Schedulers.io());
    }
}
