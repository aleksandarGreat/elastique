package archon.solutions.com.elastique.ui.fragment;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import archon.solutions.com.elastique.R;
import io.reactivex.disposables.CompositeDisposable;

public class BaseFragment extends Fragment {

    protected static final String TAG = BaseFragment.class.getSimpleName();

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void onComplete() {
    }

    protected void onError(Throwable e) {
        onError(e, e.getMessage());
    }

    protected void onError(Throwable e, String message) {
        if (message == null) {
            message = getString(R.string.unknown_error);
        }

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        Log.e(TAG, message, e);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
