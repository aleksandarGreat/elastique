package archon.solutions.com.elastique.repository.remote.dto;

import java.util.ArrayList;
import java.util.List;

public class SearchBookDtoList extends BookDtoList {

    private static final int DEFAULT_OFFSET = 0;
    private static final int DEFAULT_LIMIT = 50;
    private static final int DEFAULT_TOTAL = 0;

    private long offset;

    private long limit;

    private long total;

    public SearchBookDtoList(List<BookDto> bookDtos, long offset, long limit, long total) {
        super(bookDtos);
        this.offset = offset;
        this.limit = limit;
        this.total = total;
    }

    public static SearchBookDtoList constructDefault() {
        return new SearchBookDtoList(new ArrayList<>(), DEFAULT_OFFSET, DEFAULT_LIMIT, DEFAULT_TOTAL);
    }

    public static SearchBookDtoList constructDefaultWithBookDtos(List<BookDto> bookDtos) {
        return new SearchBookDtoList(bookDtos, DEFAULT_OFFSET, DEFAULT_LIMIT, DEFAULT_TOTAL);
    }

    public long getOffset() {
        return offset;
    }

    public long getLimit() {
        return limit;
    }

    public long getTotal() {
        return total;
    }
}
