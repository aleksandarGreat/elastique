package archon.solutions.com.elastique.system;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import archon.solutions.com.elastique.repository.remote.dependency.NetworkManager;

public class NetworkManagerImpl implements NetworkManager {

    private final Context context;

    public NetworkManagerImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }

        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
