package archon.solutions.com.elastique.repository.remote.interceptor;

import android.support.annotation.NonNull;

import java.io.IOException;

import archon.solutions.com.elastique.repository.remote.dependency.NetworkManager;
import okhttp3.Interceptor;
import okhttp3.Response;

public class HttpInterceptor implements Interceptor {

    private final NetworkManager networkManager;

    public HttpInterceptor(NetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        final Response response;
        if (!networkManager.isNetworkAvailable()) {
            throw new NoNetworkException();
        }
        response = chain.proceed(chain.request());
        return response;
    }
}
