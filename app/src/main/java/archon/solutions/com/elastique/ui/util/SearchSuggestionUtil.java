package archon.solutions.com.elastique.ui.util;

import java.util.ArrayList;
import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;

public class SearchSuggestionUtil {

    public static String[] getBookTitlesFromBooksForSuggestions(List<Book> books) {
        List<String> searchSuggestions = new ArrayList<>();

        for (Book book : books) {
            searchSuggestions.add(book.getTitle());
        }

        return searchSuggestions.toArray(new String[searchSuggestions.size()]);
    }
}
