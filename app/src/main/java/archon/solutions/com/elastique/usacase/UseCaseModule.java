package archon.solutions.com.elastique.usacase;

import javax.inject.Singleton;

import archon.solutions.com.elastique.usacase.repository.BookUseCase;
import archon.solutions.com.elastique.usacase.repository.dependency.remote.BookRemoteDao;
import dagger.Module;
import dagger.Provides;

@Module
public class UseCaseModule {

    @Provides
    @Singleton
    BookUseCase provideBookUseCase(BookRemoteDao bookRemoteDao) {
        return new BookUseCase(bookRemoteDao);
    }
}
