package archon.solutions.com.elastique.repository.remote.dao;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.repository.remote.client.BackendApiService;
import archon.solutions.com.elastique.repository.remote.converter.BookDtoToBookConverter;
import archon.solutions.com.elastique.usacase.repository.dependency.remote.BookRemoteDao;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class BookRemoteDaoImpl implements BookRemoteDao {

    private final BackendApiService backendApiService;

    public BookRemoteDaoImpl(BackendApiService backendApiService) {
        this.backendApiService = backendApiService;
    }

    @Override
    public Flowable<List<Book>> readHighlightedBooks() {
        return backendApiService.readHighlightedBooks()
                .map(BookDtoToBookConverter::convertBooksListDtoToBooks).subscribeOn(Schedulers.io());
    }

    @Override
    public Flowable<List<Book>> readBooksByQuery(String query) {
        return backendApiService.readBooksByQuery(query)
                .map(BookDtoToBookConverter::convertBooksListDtoToBooks).subscribeOn(Schedulers.io());
    }
}
