package archon.solutions.com.elastique.repository.remote.interceptor;

import archon.solutions.com.elastique.ElastiqueApplication;
import archon.solutions.com.elastique.R;

public class NoNetworkException extends RuntimeException {

    @Override
    public String getMessage() {
        return ElastiqueApplication.getContext().getResources().getString(R.string.no_network);
    }
}
