package archon.solutions.com.elastique.repository.remote.client;

import javax.inject.Inject;

import archon.solutions.com.elastique.repository.remote.dependency.NetworkManager;
import archon.solutions.com.elastique.repository.remote.interceptor.HttpInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient implements HttpClient {

    private static final String API_URL = "http://assessment.elastique.nl/";

    private final BackendApiService backendApiService;

    @Inject
    public RetrofitClient(NetworkManager networkManager) {

        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .addInterceptor(new HttpInterceptor(networkManager))
                .addNetworkInterceptor(loggingInterceptor);

        final OkHttpClient client = okHttpBuilder.build();

        final Retrofit backendApiRetrofit = new Retrofit.Builder().baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

        backendApiService = backendApiRetrofit.create(BackendApiService.class);
    }

    @Override
    public BackendApiService getBackendApiService() {
        return backendApiService;
    }
}
