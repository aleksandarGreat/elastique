package archon.solutions.com.elastique.repository.remote.dependency;

public interface NetworkManager {

    boolean isNetworkAvailable();
}
