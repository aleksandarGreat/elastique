package archon.solutions.com.elastique.repository.remote.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BookDtoList {

    @SerializedName("books")
    private List<BookDto> bookDtos;

    public BookDtoList(List<BookDto> bookDtos) {
        this.bookDtos = bookDtos;
    }

    public static BookDtoList constructDefault() {
        return new BookDtoList(new ArrayList<>());
    }

    public List<BookDto> getBooks() {
        return bookDtos;
    }
}
