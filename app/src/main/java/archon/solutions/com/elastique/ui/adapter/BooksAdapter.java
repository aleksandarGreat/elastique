package archon.solutions.com.elastique.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.ui.adapter.generic.RecyclerViewAdapterBase;
import archon.solutions.com.elastique.ui.adapter.generic.ViewWrapper;
import archon.solutions.com.elastique.ui.view.BookItemView;
import archon.solutions.com.elastique.ui.view.BookItemView_;

@EBean
public class BooksAdapter extends RecyclerViewAdapterBase<Book, BookItemView> {

    @RootContext
    Context context;

    private BookItemView.BookActionListener bookActionListener;

    @Override
    protected BookItemView onCreateItemView(ViewGroup parent, int viewType) {
        return BookItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewWrapper<BookItemView> holder, int position) {
        holder.getView().bind(items.get(position), bookActionListener);
    }

    public void setBooks(List<Book> books) {
        this.items = books;
        notifyDataSetChanged();
    }

    public void setBookActionListener(BookItemView.BookActionListener bookActionListener) {
        this.bookActionListener = bookActionListener;
    }
}
