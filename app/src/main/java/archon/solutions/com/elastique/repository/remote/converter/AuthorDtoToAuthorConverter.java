package archon.solutions.com.elastique.repository.remote.converter;


import java.util.ArrayList;
import java.util.List;

import archon.solutions.com.elastique.domain.model.Author;
import archon.solutions.com.elastique.repository.remote.dto.AuthorDto;

public class AuthorDtoToAuthorConverter {

    public static List<Author> authorDtosToAuthorsConverter(List<AuthorDto> authorsDtos) {
        List<Author> authors = new ArrayList<>();

        for (AuthorDto authorDto : authorsDtos) {
            authors.add(authorDtoToAuthorConverter(authorDto));
        }

        return authors;
    }

    static Author authorDtoToAuthorConverter(AuthorDto authorDto) {
        return new Author(authorDto.getId(), authorDto.getFirstName(), authorDto.getLastName());
    }
}
