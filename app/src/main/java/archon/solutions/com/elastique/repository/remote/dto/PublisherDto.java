package archon.solutions.com.elastique.repository.remote.dto;

public class PublisherDto {

    private long id;

    private String name;

    public PublisherDto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
