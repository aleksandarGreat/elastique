package archon.solutions.com.elastique;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

import org.androidannotations.annotations.EApplication;

import archon.solutions.com.elastique.system.SystemModule;

@SuppressLint("Registered")
@EApplication
public class ElastiqueApplication extends Application {

    private static ElastiqueApplication instance;
    private DependencyInjectionComponent diComponent;

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        diComponent = DaggerDependencyInjectionComponent.builder()
                .systemModule(new SystemModule(this)).build();

        Fresco.initialize(this);
    }

    public DependencyInjectionComponent getDiComponent() {
        return diComponent;
    }
}
