package archon.solutions.com.elastique.ui.fragment;

import android.net.Uri;
import android.text.Html;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import archon.solutions.com.elastique.R;
import archon.solutions.com.elastique.domain.model.Book;

@EFragment(R.layout.fragment_book)
public class BookFragment extends BaseFragment {

    @ViewById
    TextView title;

    @ViewById
    TextView isbn;

    @ViewById
    TextView author;

    @ViewById
    TextView description;

    @ViewById
    TextView publisher;

    @ViewById
    SimpleDraweeView image;

    @FragmentArg
    Book book;

    @AfterViews
    void initialize() {
        title.setText(book.getTitle());
        isbn.setText(Html.fromHtml(getString(R.string.isbn, book.getIsbn())));
        author.setText(Html.fromHtml(getString(R.string.author, book.getAuthor().getFirstName(),
                book.getAuthor().getLastName())));
        publisher.setText(Html.fromHtml(getString(R.string.publisher, book.getPublisher().getName())));
        image.setImageURI(Uri.parse(book.getCoverUrl()));
        description.setText(getString(R.string.description, book.getDescription()));
    }
}
