package archon.solutions.com.elastique.domain.model;

import java.io.Serializable;

public class Book implements Serializable {

    private long id;

    private String title;

    private String description;

    private String coverUrl;

    private String isbn;

    private Author author;

    private Publisher publisher;

    public Book(long id, String title, String description, String coverUrl, String isbn) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.coverUrl = coverUrl;
        this.isbn = isbn;
    }

    public Book(long id, String title, String description, String coverUrl, String isbn, Author author, Publisher publisher) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.coverUrl = coverUrl;
        this.isbn = isbn;
        this.author = author;
        this.publisher = publisher;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public String getIsbn() {
        return isbn;
    }

    public Author getAuthor() {
        return author;
    }

    public Publisher getPublisher() {
        return publisher;
    }
}
