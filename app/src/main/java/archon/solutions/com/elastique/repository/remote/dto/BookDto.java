package archon.solutions.com.elastique.repository.remote.dto;

import com.google.gson.annotations.SerializedName;

public class BookDto {

    private long id;

    private String title;

    private String description;

    @SerializedName("cover_url")
    private String coverUrl;

    private String isbn;

    private AuthorDto author;

    private PublisherDto publisher;

    public BookDto(long id, String title, String description, String coverUrl, String isbn, AuthorDto author, PublisherDto publisher) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.coverUrl = coverUrl;
        this.isbn = isbn;
        this.author = author;
        this.publisher = publisher;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public String getIsbn() {
        return isbn;
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public PublisherDto getPublisher() {
        return publisher;
    }
}
