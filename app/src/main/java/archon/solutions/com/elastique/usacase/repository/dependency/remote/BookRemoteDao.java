package archon.solutions.com.elastique.usacase.repository.dependency.remote;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import io.reactivex.Flowable;

public interface BookRemoteDao {

    Flowable<List<Book>> readHighlightedBooks();

    Flowable<List<Book>> readBooksByQuery(String query);
}
