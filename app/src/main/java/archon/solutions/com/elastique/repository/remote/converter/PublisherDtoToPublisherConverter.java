package archon.solutions.com.elastique.repository.remote.converter;

import java.util.ArrayList;
import java.util.List;

import archon.solutions.com.elastique.domain.model.Publisher;
import archon.solutions.com.elastique.repository.remote.dto.PublisherDto;

public class PublisherDtoToPublisherConverter {

    static List<Publisher> publisherDtosToPublishers(List<PublisherDto> publisherDtos) {
        List<Publisher> publishers = new ArrayList<>();

        for (PublisherDto publisherDto : publisherDtos) {
            publishers.add(publisherDtoToPublisher(publisherDto));
        }

        return publishers;
    }

    static Publisher publisherDtoToPublisher(PublisherDto publisherDto) {
        return new Publisher(publisherDto.getId(), publisherDto.getName());
    }
}
