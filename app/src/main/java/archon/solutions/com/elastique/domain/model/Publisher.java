package archon.solutions.com.elastique.domain.model;

import java.io.Serializable;

public class Publisher implements Serializable {

    private long id;

    private String name;

    public Publisher(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
