package archon.solutions.com.elastique.ui.view;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import archon.solutions.com.elastique.R;
import archon.solutions.com.elastique.domain.model.Book;

@EViewGroup(R.layout.item_view_book)
public class BookItemView extends CardView {

    @ViewById
    TextView title;

    @ViewById
    TextView author;

    @ViewById
    TextView publisher;

    @ViewById
    SimpleDraweeView image;

    private Book book;
    private BookActionListener bookActionListener;

    public BookItemView(Context context) {
        super(context);
    }

    public void bind(Book book, BookActionListener bookActionListener) {
        this.book = book;
        this.bookActionListener = bookActionListener;

        title.setText(book.getTitle());
        author.setText(Html.fromHtml(getResources().getString(R.string.author, book.getAuthor().getFirstName(),
                book.getAuthor().getLastName())));
        publisher.setText(Html.fromHtml(getResources().getString(R.string.publisher, book.getPublisher().getName())));
        image.setImageURI(Uri.parse(book.getCoverUrl()));
    }

    @Click
    void card() {
        if (bookActionListener != null) {
            bookActionListener.bookSelected(book);
        }
    }

    /**
     * Handles actions in books list.
     */
    public interface BookActionListener {

        /**
         * Handles tap on a book.
         *
         * @param book Tapped event.
         */
        void bookSelected(Book book);
    }
}
