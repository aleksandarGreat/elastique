package archon.solutions.com.elastique.system;

import android.content.Context;

import javax.inject.Singleton;

import archon.solutions.com.elastique.repository.remote.dependency.NetworkManager;
import dagger.Module;
import dagger.Provides;

@Module
public class SystemModule {

    private final Context context;

    public SystemModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    NetworkManager providesNetworkManagerProvider() {
        return new NetworkManagerImpl(context);
    }
}
