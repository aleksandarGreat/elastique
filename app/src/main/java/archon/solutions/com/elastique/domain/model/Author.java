package archon.solutions.com.elastique.domain.model;

import java.io.Serializable;

public class Author implements Serializable {

    private long id;

    private String firstName;

    private String lastName;

    public Author(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
