package archon.solutions.com.elastique.ui.activity;

import android.annotation.SuppressLint;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;

import archon.solutions.com.elastique.R;
import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.ui.fragment.BookFragment;
import archon.solutions.com.elastique.ui.fragment.BookFragment_;

@SuppressLint("Registered")
@EActivity(R.layout.activity_book)
public class BookActivity extends AppCompatActivity {

    @FragmentById
    BookFragment bookFragment;

    @Extra
    Book book;

    @AfterViews
    void initialize() {
        createBookFragment();
    }

    private void createBookFragment() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bookFragment = BookFragment_.builder().book(book).build();
        fragmentTransaction.replace(R.id.bookFragment, bookFragment);
        fragmentTransaction.commit();
    }
}
