package archon.solutions.com.elastique.repository.remote.client;

import archon.solutions.com.elastique.repository.remote.dto.BookDtoList;
import archon.solutions.com.elastique.repository.remote.dto.SearchBookDtoList;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BackendApiService {

    @GET("books/highlighted")
    Flowable<BookDtoList> readHighlightedBooks();

    @GET("books/search/{query}")
    Flowable<SearchBookDtoList> readBooksByQuery(@Path("query") String query);
}
