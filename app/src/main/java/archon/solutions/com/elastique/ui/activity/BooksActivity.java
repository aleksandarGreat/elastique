package archon.solutions.com.elastique.ui.activity;

import android.annotation.SuppressLint;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import archon.solutions.com.elastique.R;
import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.ui.fragment.BooksFragment;

import static archon.solutions.com.elastique.ui.util.SearchSuggestionUtil.getBookTitlesFromBooksForSuggestions;

@SuppressLint("Registered")
@EActivity(R.layout.activity_books)
@OptionsMenu(R.menu.options_menu_search)
public class BooksActivity extends AppCompatActivity implements BooksFragment.BooksFragmentListener {

    private final List<Book> books = new ArrayList<>();
    @FragmentById
    BooksFragment booksFragment;
    @ViewById
    Toolbar toolbar;
    @ViewById
    MaterialSearchView searchView;

    @AfterViews
    void initialize() {
        setSupportActionBar(toolbar);
        booksFragment.setRetainInstance(true);
        booksFragment.setBooksFragmentListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.actionSearch);
        searchView.setMenuItem(item);
        searchView.setSubmitOnClick(true);
        setupMaterialSearchView();
        return true;
    }

    @OptionsItem(android.R.id.home)
    void home() {
        booksFragment.readHighlightedBooks();
        setSupportActionBarHomeAsUpEnabled(false);
    }

    private void setupMaterialSearchView() {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!checkBooksAndNavigate(query)) {
                    booksFragment.readBooksByQuery(query);
                    setSupportActionBarHomeAsUpEnabled(true);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private boolean checkBooksAndNavigate(String titleQuery) {
        for (Book book : books) {
            if (book.getTitle().equals(titleQuery)) {
                BookActivity_.intent(getBaseContext()).book(book).start();
                return true;
            }
        }

        return false;
    }

    private void setSupportActionBarHomeAsUpEnabled(boolean enabled) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else if (getSupportActionBar() != null &&
                (getSupportActionBar().getDisplayOptions() & ActionBar.DISPLAY_HOME_AS_UP) != 0) {
            booksFragment.readHighlightedBooks();
            setSupportActionBarHomeAsUpEnabled(false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void booksUpdated(List<Book> books) {
        this.books.addAll(books);
        searchView.setSuggestions(getBookTitlesFromBooksForSuggestions(books));
    }
}
