package archon.solutions.com.elastique.repository.generator.dto;

import archon.solutions.com.elastique.repository.generator.Generator;
import archon.solutions.com.elastique.repository.remote.dto.BookDtoList;

public class BookListDtoGenerator extends Generator<BookDtoList> {

    private final int numberOfBookDtos;

    private BookDtoGenerator bookDtoGenerator = new BookDtoGenerator();

    public BookListDtoGenerator(int numberOfBookDtos) {
        this.numberOfBookDtos = numberOfBookDtos;
    }

    @Override
    public BookDtoList next() {
        return new BookDtoList(bookDtoGenerator.getList(numberOfBookDtos));
    }
}
