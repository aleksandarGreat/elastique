package archon.solutions.com.elastique.repository.remote.httpclient.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import archon.solutions.com.elastique.domain.model.Book;
import archon.solutions.com.elastique.repository.generator.dto.BookListDtoGenerator;
import archon.solutions.com.elastique.repository.generator.dto.SearchBookListDtoGenerator;
import archon.solutions.com.elastique.repository.remote.client.BackendApiService;
import archon.solutions.com.elastique.repository.remote.converter.BookDtoToBookConverter;
import archon.solutions.com.elastique.repository.remote.dao.BookRemoteDaoImpl;
import archon.solutions.com.elastique.repository.remote.dto.BookDtoList;
import archon.solutions.com.elastique.repository.remote.dto.SearchBookDtoList;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookRemoteDaoImplTest {

    private static final int NUMBER_OF_BOOKS_TO_GENERATE = 3;

    @Mock
    private BackendApiService backendApiService;

    private BookRemoteDaoImpl bookremoteDaoImpl;

    private BookListDtoGenerator bookListDtoGenerator = new BookListDtoGenerator(NUMBER_OF_BOOKS_TO_GENERATE);

    private SearchBookListDtoGenerator searchBookListDtoGenerator = new SearchBookListDtoGenerator(NUMBER_OF_BOOKS_TO_GENERATE);

    @Before
    public void setup() {
        bookremoteDaoImpl = new BookRemoteDaoImpl(backendApiService);
    }

    @Test
    public void testReadHighlighted() {
        // Arrange
        final BookDtoList bookDtoList = bookListDtoGenerator.next();
        when(backendApiService.readHighlightedBooks()).thenReturn(Flowable.just(bookDtoList));

        // Act
        final TestSubscriber<List<Book>> testObserver = bookremoteDaoImpl.readHighlightedBooks().test();
        testObserver.awaitTerminalEvent();
        final List<Book> testBooks = testObserver.values().get(0);

        // Assert
        testObserver.assertNoErrors().assertValue(testObserverBooks ->
                testObserverBooks.size() == bookDtoList.getBooks().size());

        assertThat(testBooks.get(0).getId(), equalTo(BookDtoToBookConverter
                .convertBookDtoToBook(bookDtoList.getBooks().get(0)).getId()));
        assertThat(testBooks.get(1).getId(), equalTo(BookDtoToBookConverter
                .convertBookDtoToBook(bookDtoList.getBooks().get(1)).getId()));
        assertThat(testBooks.get(2).getId(), equalTo(BookDtoToBookConverter
                .convertBookDtoToBook(bookDtoList.getBooks().get(2)).getId()));

        verify(backendApiService, times(1)).readHighlightedBooks();
    }

    @Test
    public void testReadBooksByQuery() {
        // Arrange
        final String query = "query";
        final SearchBookDtoList searchBooksListDto = searchBookListDtoGenerator.next();
        when(backendApiService.readBooksByQuery(query)).thenReturn(Flowable.just(searchBooksListDto));

        // Act
        final TestSubscriber<List<Book>> testObserver = bookremoteDaoImpl.readBooksByQuery(query).test();
        testObserver.awaitTerminalEvent();
        final List<Book> testBooks = testObserver.values().get(0);

        // Assert
        testObserver.assertNoErrors().assertValue(testObserverBooks ->
                testObserverBooks.size() == searchBooksListDto.getBooks().size());

        assertThat(testBooks.get(0).getId(), equalTo(BookDtoToBookConverter
                .convertBookDtoToBook(searchBooksListDto.getBooks().get(0)).getId()));
        assertThat(testBooks.get(1).getId(), equalTo(BookDtoToBookConverter
                .convertBookDtoToBook(searchBooksListDto.getBooks().get(1)).getId()));
        assertThat(testBooks.get(2).getId(), equalTo(BookDtoToBookConverter
                .convertBookDtoToBook(searchBooksListDto.getBooks().get(2)).getId()));

        verify(backendApiService, times(1)).readBooksByQuery(query);
    }
}
