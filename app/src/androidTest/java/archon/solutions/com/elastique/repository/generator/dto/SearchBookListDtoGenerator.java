package archon.solutions.com.elastique.repository.generator.dto;

import java.util.List;

import archon.solutions.com.elastique.repository.generator.Generator;
import archon.solutions.com.elastique.repository.remote.dto.BookDto;
import archon.solutions.com.elastique.repository.remote.dto.SearchBookDtoList;

public class SearchBookListDtoGenerator extends Generator<SearchBookDtoList> {

    private final int numberOfBooks;

    public SearchBookListDtoGenerator(int numberOfBooks) {
        this.numberOfBooks = numberOfBooks;
    }

    @Override
    public SearchBookDtoList next() {
        final List<BookDto> bookDtos = new BookDtoGenerator().getList(numberOfBooks);
        return SearchBookDtoList.constructDefaultWithBookDtos(bookDtos);
    }
}
