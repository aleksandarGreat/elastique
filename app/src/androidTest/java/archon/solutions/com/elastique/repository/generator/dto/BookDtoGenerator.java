package archon.solutions.com.elastique.repository.generator.dto;

import archon.solutions.com.elastique.repository.generator.Generator;
import archon.solutions.com.elastique.repository.remote.dto.AuthorDto;
import archon.solutions.com.elastique.repository.remote.dto.BookDto;
import archon.solutions.com.elastique.repository.remote.dto.PublisherDto;

public class BookDtoGenerator extends Generator<BookDto> {

    @Override
    public BookDto next() {
        final int id = counter++;
        return new BookDto(id, "title" + id, "description" + id,
                "coverUrl" + id, "isbn" + id, new AuthorDto(id, "firstName" + id,
                "lastName" + id), new PublisherDto(id, "name" + id));
    }
}
